VM = function(){

}

VM.Instructions = new Object();
VM.Stack = new Array();

VM.Exec = function(cmd, context = null) {
  VMConsole.AddLine(cmd);

  cmd += ' ';//Ensure parsing of last command
  var args = new Array();
  var quoted = false, escaped = false;
  var parenLevel = 0;
  var buffer = '', parenBuffer = '', triggerChar = ''
  var quoteChar = '';

  var endl = cmd.length;
  for(var i = 0; i < endl; i++) {
    /*if(!escaped && cmd[i] == '(' && (triggerChar != '' || !quoted)) {
      parenLevel++;
      continue;
    } else if(!escaped && cmd[i] == ')' && parenLevel > 0) {
      parenLevel--;
      if(parenLevel == 0) {

      } else {

      }
    }*/

    if(!escaped && (cmd[i] == '"' || cmd[i] == '\'' || cmd[i] == '`') && (!quoted || quoteChar == cmd[i])) {
      quoted = !quoted;
      if(quoted) quoteChar = cmd[i];
    } else if(!escaped && cmd[i] == '\\') {
      escaped = true;
    } else if(!escaped && !quoted && cmd[i] == ' '){
      if(buffer.length > 1 && buffer[0] == '$' && Utility.IsAlpha(buffer[1])) {//Match a variable name
        args.push(eval("`${"+buffer.slice(1)+"}`"));
      } else {
        args.push(eval("`"+buffer+"`"));
      }
      buffer = '';
    } else {
      buffer += cmd[i];
      if(escaped) escaped = false;
    }
  }

  if(context != null) VM.Stack.push(context);

  //Secondary expansion
  for(var i = 0; i < args.length; i++) args[i] = eval("`"+args[i]+"`");

  //console.log("exec "+args[0]+"("+ args.slice(1) + ")");

  var inst = VM.Instructions[args[0]];
  var ret = "";
  if(inst == null) {
    ret = "err_unknown_instr";
    VMConsole.Error(args[0]+" is not a known instruction");
  } else {
    ret = inst.execute(args.slice(1));
  }

  if(context != null) VM.Stack.pop(context);

  //Feedback instruction
  while(ret != null && ret.length > 0 && ret[0] == '!') {
    ret = VM.Exec(ret.slice(1));
  }

  if(ret != null) VMConsole.AddReturn(ret);

  return ret;
}
