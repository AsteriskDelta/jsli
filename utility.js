Utility = function() {

}

HTMLEntityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

Utility.EscapeHTML = function(string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return HTMLEntityMap[s];
  });
}

Utility.Romanize = function(num) {
  if (!+num) return NaN;
  var digits = String(+num).split(""),
      key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
              "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
             "","I","II","III","IV","V","VI","VII","VIII","IX"],
      roman = "",
      i = 3;
    while (i--) roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
}

Utility.IsAlpha = function(ch){
  return /^[A-Z]$/i.test(ch);
}

Utility.Decode = function(input) {
  var doc = new DOMParser().parseFromString(input, "text/html");
  return doc.documentElement.textContent;
}

Utility.Sleep = function(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

Color = function() {
  this.r = this.g = this.b = 0;
  this.a = 255;
  this.data = function() {
    return [this.r,this.g,this.b,this.a];
  }
  this.component = function(index, val) {
    if(index == 0) this.r = val;
    else if(index == 1) this.g = val;
    else if(index == 2) this.b = val;
    else if(index == 3) this.a = val;
  }
  this.rgb = function() {
    return this.data().slice(0,3);
  }
  this.rgba = function() {
    return this.data();
  }

  this.css = function() {
    return `rgba(${this.rgba().join(',')})`;
  }
}

Color.From = function(css) {
  var ret = new Color();
  for(var i = 0; i < 4; i++) ret.component(i, css.match(/\d+/)[i]);
  return ret;
}

//Why do I even have to do this...
Array.prototype.resize = function(newSize, defaultValue) {
    while(newSize > this.length)
        this.push(defaultValue);
    this.length = newSize;
}
