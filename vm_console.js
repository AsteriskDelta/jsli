function VMConsole() {

}

VMConsole.Shown = false;
VMConsole.HistoryMax = 100;

VMConsole.History = new Array();
VMConsole.Contents = new Array();
VMConsole.HistoryOff = -1;

VMConsole.show = function() {
  $("#console").removeClass("hidden");
  $("#btConsole").addClass("btActive");
  $("#consolePrefix").html("["+ Auth.User + "@" + Auth.Platform + "]$ ");
  VMConsole.Shown = true;
};

VMConsole.hide = function() {
  $("#console").addClass("hidden");
  $("#btConsole").removeClass("btActive");
  VMConsole.Shown = false;
};

VMConsole.toggle = function() {
  if(VMConsole.Shown) VMConsole.hide();
  else VMConsole.show();
}

VMConsole.Render = function() {
  while(VMConsole.History.length > 30) VMConsole.History.shift();

  while(VMConsole.Contents.length > 100) VMConsole.Contents.shift();

  var rawHtml = "";
  var len = VMConsole.Contents.length;
  for(var i = 0; i < len; i++) {
    rawHtml += VMConsole.Contents[i] + "<br />";
  }
  $("#consoleTextInner").html(rawHtml);
}

VMConsole.Exec = function(str) {
  VMConsole.AddHistory(str);
  VM.Exec(str);
}

VMConsole.AddLine = function(line) {
  //VMConsole.History.push(Utility.EscapeHTML(line));
  VMConsole.Contents.push(Utility.EscapeHTML(line));
  VMConsole.Render();
}
VMConsole.AddHistory = function(line) {
  VMConsole.History.push(Utility.EscapeHTML(line));
  VMConsole.Render();
}

VMConsole.Error = function(err) {
  VMConsole.AddReturn("[ERR] "+err);
}

VMConsole.Print = function(text) {
  VMConsole.AddReturn(text);
}

VMConsole.AddReturn = function(data) {
  VMConsole.Contents.push(Utility.EscapeHTML(data));
  VMConsole.Render();
}

$(document).on( 'keyup', '#consoleInputEditable', function(event) {
  if (event.keyCode === 13) {
    VMConsole.Exec($("#consoleInputEditable").text());
    $("#consoleInputEditable").text("");
    VMConsole.HistoryOff = -1;
  } else if(event.keyCode === 38) {
    VMConsole.HistoryOff = Math.min(VMConsole.HistoryOff+1, VMConsole.History.length - 1);
    $("#consoleInputEditable").text(Utility.Decode(VMConsole.History[VMConsole.History.length - VMConsole.HistoryOff - 1]));
  } else if(event.keyCode === 40) {
    if(VMConsole.HistoryOff <= 0) {
      VMConsole.HistoryOff = -1;
      $("#consoleInputEditable").text("");
      return;
    }
    VMConsole.HistoryOff = Math.max(0, VMConsole.HistoryOff - 1);
    $("#consoleInputEditable").text(Utility.Decode(VMConsole.History[VMConsole.History.length - VMConsole.HistoryOff - 1]));
  }
});
