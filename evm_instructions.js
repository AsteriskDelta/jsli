VM.Instruction = function() {
  this.id = "";
  this.help = "";
  this.execute = function(args, context = null) {

  }
}

VM.Instructions.exec = new function() {
  this.id = "exec";
  this.help = "executes the concatenated arguments as a command";
  this.execute = function(args, context = null) {
    if(args == null) return;
    VM.Exec(args.join(' '));
  }
}

VM.Instructions.set = new function() {
  this.id = "set";
  this.help = "sets the variable to the value of remaining arguments (evaluated)";
  this.execute = function(args, context = null) {
    if(args == null) return;
    txt = args.slice(1).join(' ');
    eval(`${args[0]} = ${txt};`);
  }
}
VM.Instructions.get = new function() {
  this.id = "get";
  this.help = "gets the specified variable";
  this.execute = function(args, context = null) {
    if(args == null) return;
    txt = Array.prototype.join.call(args, ' ');
    return eval(`${txt};`);
  }
}

VM.Instructions.story = new function() {
  this.id = "story";
  this.help = "prints arguments as text";
  this.execute = function(args, context = null) {
    if(args == null) return;
    for(var i = 0; i < args.length; i++) Reader.Display(args[i]);
  }
}

VM.Instructions.choice = new function() {
  this.id = "choice";
  this.help = "prints arguments as text";
  this.execute = function(args, context = null) {
    if(args == null) return;

    var contents = "<ul>";
    for(var i = 0; i < args.length; i++) {
      contents += "<li onclick=\"$(this).addClass('active');\">" + args[i] + "</li>";
    }
    contents += "</ul>"
    Reader.Display(contents);
  }
}

VM.Instructions.apply = new function() {
  this.id = "apply";
  this.help = "apply [stat] [entity ID] [dmg] [[type]]<br />damages the entity (by ID) for of an optional type";
  this.execute = function(args, context = null) {
    var stat = args[0];
    var target = Entity.Find(args[1]);
    if(target == null) {
      VMConsole.Error("Unable to find entity with id \""+args[1]+"\"");
      return false;
    }

    var dmgType = "";
    if(args.length > 2) dmgType = args[3];

    var fnName = "apply_"+stat;
    /*if(fnName in target) {
      target[fnName](parseInt(args[2]), dmgType);
    } else */{//Generic call
      target.apply(stat, parseFloat(args[2]), dmgType);
    }
  }

  VM.Instructions.inflict = new function() {
    this.id = "inflict";
    this.help = "inflict [effect] [[duration]] [[strength]]<br />inflicts the given effect for $duration turns (if applicable), at effect level $strength";
    this.execute = function(args, context = null) {
      var target = Entity.Find(args[1]);
      if(target == null) {
        VMConsole.Error("Unable to find entity with id \""+args[1]+"\"");
        return false;
      }

      var effect = "";
      var duration = 0;
      var strength = 0;
      if(args.length > 0) effect = args[0];
      if(args.length > 1) duration = parseFloat(args[1]);
      if(args.length > 2) strength = parseFloat(args[2]);

      target.inflict(effect, duration, strength);
    }
  }

  VM.Instructions.move = new function() {
    this.id = "move";
    this.help = "move [entity ID] [dx] [dy]<br />Moves the entity by dx,dy";
    this.execute = function(args, context = null) {
      var target = Entity.Find(args[1]);
      if(target == null) {
        VMConsole.Error("Unable to find entity with id \""+args[1]+"\"");
        return false;
      }

      var del = new Vec2();
      del.x = parseInt(args[1]);
      del.y = parseInt(args[2]);

      target.move(del)
    }
  }
}
