Draw = function() {

}
Draw.Objects = new Object();

Draw.StaticID = 0;
Draw.GetID = function() {
    var ret = Draw.StaticID++;
    return "d_"+ret;
};

Draw.AddObject = function(id, obj, parent) {
    Draw.Objects[parent][id] = obj;
}

Draw.Clear = function(parent = "body") {
    for (var key in Draw.Objects[parent]) {
        if (Draw.Objects[parent].hasOwnProperty(key)) {
            $(key).remove();
        }
    }
};

Draw.GetOffset = function(el, parent) {
    var rect = el[0].getBoundingClientRect();
    var parentRect = $(parent)[0].getBoundingClientRect();
    return {
        left: rect.left + window.pageXOffset - parentRect.left,
        top: rect.top + window.pageYOffset - parentRect.top,
        width: rect.width || el.offsetWidth,
        height: rect.height || el.offsetHeight
    };
};

Draw.Line = function(x1, y1, x2, y2, color, thickness, parent = "body") {
    //Distance
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    //Center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    //Angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);
    //Make hr
    var newID = Draw.GetID();
    var htmlLine = "<div id='" + newID + "' style='padding:0px; margin:0px; height:" + thickness + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
    //alert(htmlLine);
    $(parent).append(htmlLine);
    return newID;
};

Draw.GetConnection = function(div1, div2, parent) {
    var off1 = Draw.GetOffset(div1, parent);
    var off2 = Draw.GetOffset(div2, parent);

    //Center 1
    var x1 = off1.left + off1.width/2;
    var y1 = off1.top + off1.height/2;
    //Center 2
    var x2 = off2.left + off2.width/2;
    var y2 = off2.top + off2.height/2;

    //Draw.Circle(x1, y1, 10, "black", 3, parent);
    //Draw.Circle(x2, y2, 10, "black", 3, parent);

    //The div's respective dels
    var d1 = {x: x2 - x1, y: y2 - y1};
    var d2 = {x: -d1.x, y: -d1.y};

    var r1 = Math.sqrt(off1.width*off1.width/4 + off1.height*off1.height/4);
    var r2 = Math.sqrt(off2.width*off2.width/4 + off2.height*off2.height/4);

    //Draw.Circle(x1, y1, r1*2, "black", 2, parent);
    //Draw.Circle(x2, y2, r2*2, "black", 2, parent);
    //console.log(Geometry.Normalize(d1));
    //console.log(Geometry.Normalize(d2));

    var p1 = Geometry.Mul(Geometry.Normalize(d1), r1);
    var p2 = Geometry.Mul(Geometry.Normalize(d2), r2);

    //console.log(p1);
    //console.log(p2);

    p1.x += x1;
    p1.y += y1;
    p2.x += x2;
    p2.y += y2;
    return {start:{x:p1.x,y:p1.y}, end:{x:p2.x,y:p2.y}}
}

Draw.Connect = function(div1, div2, color, thickness, parent = "body") {
    var result = Draw.GetConnection(div1, div2, parent);
    var s = result.start;
    var e = result.end;

    return Draw.Line(s.x, s.y, e.x, e.y, color, thickness, parent);
};

Draw.Direct = function(div1, div2, color, thickness, parent = "body") {
    var result = Draw.GetConnection(div1, div2, parent);
    var s = result.start;
    var e = result.end;

    return Draw.Arrow(s.x, s.y, e.x, e.y, color, thickness, parent);
};

Draw.Rect = function(x, y, width, height, color, thickness, parent = "body", angle = 0) {
    var newID = Draw.GetID();
    var cx = x - width/2;
    var cy = y - height/2;

    var htmlLine = "<div id='" + newID + "' style='padding:0px; margin:0px; height:" + height + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + width + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";

    $(parent).append(htmlLine);
    return newID;
};

Draw.Circle = function(x, y, diameter, color, thickness, parent = "body") {
    var newID = Draw.GetID();
    var height = diameter;
    var width = diameter;
    var radius = diameter / 2;

    var cx = x - width/2;
    var cy = y - height/2;

    console.log(cx + ", " + cy);

    var htmlLine = "<div id='" + newID + "' style='padding:0px; margin:0px; height:" + height + "px; border: " + thickness + "px solid " + color + "; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + width + "px; border-radius: " + radius + "px;' />";

    $(parent).append(htmlLine);
    return newID;
};

Draw.Text = function(x, y, width, height, color, thickness, text, parent = "body", angle = 0) {
    var newID = Draw.GetID();
    var cx = x - width/2;
    var cy = y - height/2;

    var htmlLine = "<div id='" + newID + "' style='padding:0px; margin:0px; height:" + height + "px; color:" + color + "; text-align: center; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + width + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' >" + text + "</div>";

    $(parent).append(htmlLine);
    return newID;
};

Draw.Arrow = function(x1, y1, x2, y2, color, thickness, parent = "body") {
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    //Center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    //Angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);

    var sz = thickness * 3 + 6;
    var tEdge = " " + sz + "px solid rgba(0,0,0,0);";
    var vEdge = " " + sz + "px solid " + color +";";


    var newID = Draw.GetID();
    var htmlLine = "<div id='" + newID + "' style='padding:0px; margin:0px; height:" + thickness + "px; background-color:" + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg); overflow:visable;' >";
    htmlLine += "</div><div style=\"border: " + tEdge + "; border-right: "  + vEdge +"; width:"+0+"; height:"+0+"; position:absolute; left:"+(x2-sz)+";top:"+(y2-sz)+"; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);\" />";
    htmlLine += "";

    $(parent).append(htmlLine);
    return newID;
};
