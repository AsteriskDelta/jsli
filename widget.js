Widget = function() {
  this.id = "";
  this.update = null;
  this.source = null;
}

Widget.Widgets = new Array();

Widget.Initialize = function() {

}

Widget.Widgets.Redraw = function() {
  var mi = Widget.Widgets.length;
  for(var i = 0; i < mi; i++) {
    if(Widget.Widgets[i].update != null) Widget.Widgets[i].update();
  }
}


TimeWidget = function() {
  this.prototype = Widget;
  this.id = "timeWidget";
  this.update = function() {
    $("#timeReadout").html(Game.Time.pretty());
  }
}

MoneyWidget = function() {
  this.prototype = Widget;
  this.id = "moneyWidget";
  this.update = function() {
    $("#moneyReadout").html("Gold: 0");
  }
}
