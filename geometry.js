Geometry = function() {

}

Geometry.Length = function(vec) {
    return Math.sqrt(vec.x*vec.x + vec.y*vec.y);
}

Geometry.Distance = function(p1, p2) {
    var del = {x: p2.x - p1.x, y: p2.y - p1.y};
    return Geometry.Length(del);
}

Geometry.Mul = function(vec, scaler) {
    return {x: vec.x*scaler, y: vec.y*scaler};
}

Geometry.Div = function(vec, scaler) {
    return {x: vec.x/scaler, y: vec.y/scaler};
}

Geometry.Mul2D = function(vec, vecb) {
    return {x: vec.x*vecb.x, y: vec.y*vecb.y};
}

Geometry.Div2D = function(vec, vecb) {
    return {x: vec.x/vecb.x, y: vec.y/vecb.y};
}

Geometry.Normalize = function(vec) {
    return Geometry.Div(vec, Geometry.Length(vec));
}

Geometry.Dot = function(a, b) {
    return a.x*b.x + a.y*b.y;
}
